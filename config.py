import os 

def create_config():
    config = {}
    config['password'] = os.environ.get("PASSWORD", "TEST")
    config['delimiter'] = os.environ.get('DELIMITER', "\n").encode("UTF-8")

    return config
