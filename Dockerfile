FROM ubuntu:latest

RUN apt-get update && apt-get install -y --no-install-recommends \
        python3 \
	python3-flask \
	python3-pip 

RUN pip3 install pycryptodome

RUN useradd app

WORKDIR /www/app/

COPY handlers/ handlers/
COPY start.sh /www/app/
COPY app.py /www/app/

RUN chown app -R /www/app/
RUN chmod +x /www/app/start.sh

EXPOSE 8080 8080

ENTRYPOINT /www/app/start.sh
