import app
from app import config
import os 
import io

from flask import make_response, request, Response, abort, g, jsonify, send_file
import json

from werkzeug.wsgi import FileWrapper

from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt


def load_data():
    
    if request.json:
        message = request.json.get("message").encode('UTF-8')
        return message

    if request.files and request.files['data']:
        return request.files['data'].read()

    return None


def do_encrypt(data):
    password = config['password']
    
    salt = get_random_bytes(32)
    key = scrypt(password, salt, key_len=32, N=2**17, r=8, p=1)

    cipher = AES.new(key, AES.MODE_GCM)

    message = io.BytesIO(key + config['delimiter'] + cipher.encrypt(data))

    message.flush()
    message.seek(0)

    return message


def encrypt():

    incoming_data = load_data()
    
    if not incoming_data:
        return abort(400)

    message = do_encrypt(incoming_data)

    response = make_response(send_file(message, 
        attachment_filename='encoded', 
        as_attachment=True,
        mimetype='application/octet'))

    return response

