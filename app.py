import os
import uuid
from flask import Flask, request, g

from config import create_config

config = create_config()

def request_unique_id(response):
    uid = str(uuid.uuid4())
    response.headers['X-UUID'] = uid
    return response 


def create_app():
    global app, config

    app = Flask(__name__)#, template_folder="template_folder")

    app.after_request(request_unique_id)
    
    from handlers.encrypt import encrypt
    app.add_url_rule('/encrypt', view_func=encrypt, methods=['GET', 'POST'])

    return app


if __name__ == '__main__':
    app = create_app()
    port = os.environ.get("PORT", 8080)
    print('Starting server on %s' % port)

    app.run(host='0', port=port, use_reloader=True, debug=True)
